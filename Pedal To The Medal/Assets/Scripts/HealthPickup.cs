﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    PlayerHealth playerHealth;

    public int healthBonus;

    void Awake()
    {
        playerHealth = FindObjectOfType<PlayerHealth>(); 
    }

    void OnTriggerEnter(Collider col)
    {
        if (playerHealth.currentHealth < playerHealth.startingHealth) 
        {
            Destroy(gameObject);
            playerHealth.currentHealth = playerHealth.currentHealth + healthBonus;
        }
    }

    
}
