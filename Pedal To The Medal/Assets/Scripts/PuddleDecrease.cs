﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuddleDecrease : MonoBehaviour
{
    // Public Variable
    // Editable in Unity
    // Usable by other scripts
    public int pickupValue = 1;

    public GameObject pickupEffect;

    public int hazardDamage;

    // Called by Unity when this object overlaps with another object marked as a trigger
    // This is our condition if the player touches the coin
    public void OnTriggerEnter(Collider other)
    {
        // Check if the Score script is attached to the thing we bumped into
        Score scoreScript = other.GetComponent<Score>();

        if (scoreScript != null)
        {
            // We have a scoreScript so the thing we bumped into is the player!

            // Add our pickup value to the player's current score (actions)
            scoreScript.AddScore(pickupValue);

            Instantiate(pickupEffect, transform.position, transform.rotation);

            // we should then delete this object so we don't infinitely add score
            Destroy(gameObject);
        }
    }

    
}
