﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{
    // This will be the ammount of damage the hazard does
    // public = editable in Unity 
    // int = whole number
    public int hazardDamage;

    public GameObject pickupEffect;

    // Built-in Unity function for handling collisions
    // This function will be called when another object bumps 
    // into the one this script is attached to
    void OnCollisionEnter(Collision collisionData)
    {
        // Get the object we collided with
        Collider objectWeCollidedWith = collisionData.collider;

        // Get the PlayerHealth script attached to that object (if there is one)
        PlayerHealth player = objectWeCollidedWith.GetComponent<PlayerHealth>();

        // Check if we actually found a player health script
        // This if statement is true if the player variable is NOT null (empty)
        if (player != null)
        {
            // This means there WAS a PlayerHealth script attached to the object we bumped into
            // Which means this object is indeed the player

            Instantiate(pickupEffect, transform.position, transform.rotation);

            // Perform our on-collision action (damage the player)
            player.ChangeHealth(-hazardDamage);
        }
    }
}

