﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems; 

public class MainMenu : MonoBehaviour
{
    public MainMenu mainMenu; 

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }
    
    

    public void ExitGame ()
    {
        Debug.Log("Quit");
        Application.Quit(); 

    }

    public void Return()
    {
        SceneManager.LoadScene("MainMenu");
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Return();
        }
    }
}

