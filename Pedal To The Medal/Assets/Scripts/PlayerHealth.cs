﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class PlayerHealth : MonoBehaviour
{
    // This will be starting health for the player
    // Public - Shown in the Unity editor and accesible from other scripts
    // int = whole number
    public int startingHealth = 6;
    // Public - Can be edited in Unity
    public string gameOverScene;


    // This will be the player's current health 
    // and will change as the game goes on 
    public int currentHealth; 

    // Built-in Unity function called when the script is created
    // Usually when the game starts
    // This happens BEFORE the Start()
    void Awake()
    {
        // Initialise our current health to be equal to our 
        // starting health at the beginning of the game
        currentHealth = startingHealth; 
    }

    // NOT built into Unity
    // We must call it ourselves
    // This will change the player's current health
    // Public so other scripts can access it
    public void ChangeHealth(int changeAmount)
    {
        // Take our current health, add the change ammount, and store
        // the result back in the current health variable
        currentHealth = currentHealth + changeAmount;

        // Keep our current health between 0 and starting health value
        currentHealth = Mathf.Clamp(currentHealth, 0, startingHealth);
        
        // If our health drops to 0, that means the player should die
        if (currentHealth < 1)
        {
            // We call the Kill function to kill the player
            Kill(); 
        }
    }


    // This function is NOT built into Unity 
    // It will only be called manually by our own code 
    // It must be marked public so our other scripts can access it 
    public void Kill()
    {
        // This will destroy the gameObject that this script is attached to 
        Destroy(gameObject);

        // Load the gameover scene 
        SceneManager.LoadScene(gameOverScene); 

    }

    // This function is a custom function 
    // It is a getter function which gives info to the calling code
    // the int is the type of info that will be given 
    public int GetHealth()
    {
        // return will give the following back to the following code
        return currentHealth; 
    }

}
