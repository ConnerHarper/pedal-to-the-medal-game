﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float jumpSpeed = 1000;
    private bool isGrounded;
    public float xSpeed = 0;
    public float zSpeed = 0; 

    private Rigidbody rigidbody;

    // Variable to hold the Audio clip that should play when we walk 
    public AudioClip carMovingSound;


    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>(); 

    }

    // Update is called once per frame
    void Update()
    {

        // Get the input in the form of -1 to 1 and store
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        // Check if the player is jumping
        bool jump = Input.GetButtonDown("Jump"); 


        // Ask our current game object (that this script is attached to)
        // for any Rigidbody comment also attached to the same game object
        Rigidbody playerPhysics = GetComponent<Rigidbody>();

        // Set up the new movement vector
        Vector3 movement = playerPhysics.velocity; 

        movement.x = horizontal * xSpeed * Time.deltaTime;
   
        movement.z = vertical * zSpeed * Time.deltaTime;

        if (jump && isGrounded == true)
        {
            movement.y = jumpSpeed;
            isGrounded = false; 
        }


        // Assign the vector to the player's velocity 
        playerPhysics.velocity = movement;

 
    }

    void OnCollisionEnter()
    {
        isGrounded = true;
    }



}
