﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 

public class TimeLimit : MonoBehaviour
{

    // Public variables
    // Editable in Unity and by other projects
    public float timeLimit; // number of seconds the game lasts
    public string gameOverScene; // Scene to be loaded when time runs out

    public GameObject pickupEffect;

    // Private variables
    private float startTime; // Time when the timer started
    private Text timerDisplay; // The display for our timer


    // Start is called before the first frame update
    void Start()
    {
        // Getting our text component so we can edit the text each frame
        timerDisplay = GetComponent<Text>();

        // Set the start time for when this object was created
        startTime = Time.time; 
    }

    // Update is called once per frame
    void Update()
    {
        // Calculate how much time has passed
        float timePassed = Time.time - startTime;

        // Display time since start
        timerDisplay.text = ((int)(timeLimit - timePassed)).ToString(); 

        // Check if the time limit has been reached
        if (timePassed >= timeLimit)
        {
            // We are out of time
            // Load game over scene
            SceneManager.LoadScene(gameOverScene); 
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Pickup(other);
        }
    }

    void Pickup(Collider Player)
    {
        Instantiate(pickupEffect, transform.position, transform.rotation);


        // timeLimit.startTime = 30; 
 
        Destroy(gameObject);
    }


}
